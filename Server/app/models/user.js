//Made using Josh Morony's tutorial - https://www.joshmorony.com/creating-role-based-authentication-with-passport-in-ionic-2-part-1/

var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
 
 
 /*
 Setting variables and their properties that are required from the user
 */
var UserSchema = new mongoose.Schema({
 
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    },
	
	genre: {
		type: String,
		enum: ['pop', 'rock', 'electronic', 'classical', 'ambient', 'hip-hop']
 
	}, 
	
	dateOfBirth: {
        type: String,
        required: true
    }
 
}, {
    timestamps: true
});
 
 
 /*
 Hashing user's password using Secure Salted Password Hashing
 */
UserSchema.pre('save', function(next){
 
    var user = this;
    var SALT_FACTOR = 5;
 
    if(!user.isModified('password')){
        return next();
    } 
 
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
 
        if(err){
            return next(err);
        }
 
        bcrypt.hash(user.password, salt, null, function(err, hash){
 
            if(err){
                return next(err);
            }
 
            user.password = hash;
            next();
 
        });
 
    });
 
});

/*
Method comparing passwords
*/
 
UserSchema.methods.comparePassword = function(passwordAttempt, cb){
 
    bcrypt.compare(passwordAttempt, this.password, function(err, isMatch){
 
        if(err){
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });
 
}
 
module.exports = mongoose.model('User', UserSchema);