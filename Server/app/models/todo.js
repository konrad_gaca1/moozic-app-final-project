//Made using Josh Morony's tutorial - https://www.joshmorony.com/creating-role-based-authentication-with-passport-in-ionic-2-part-1/

var mongoose = require('mongoose');
 
var TodoSchema = new mongoose.Schema({
 
    title: {
        type: String,
        required: true
    }
 
}, {
    timestamps: true
});
 
module.exports = mongoose.model('Todo', TodoSchema);