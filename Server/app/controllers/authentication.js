//Made using Josh Morony's tutorial - https://www.joshmorony.com/creating-role-based-authentication-with-passport-in-ionic-2-part-1/

var jwt = require('jsonwebtoken');  
var User = require('../models/user');
var authConfig = require('../../config/auth');
 
 /*
 Generating JWT for the user.
 */
function generateToken(user){
    return jwt.sign(user, authConfig.secret, {
        expiresIn: 10080
    });
}
 
 
 /*
 Setting information for the JWT, does not contain password for security reasons.
 */
function setUserInfo(request){
    return {
        _id: request._id,
        email: request.email,
        role: request.role,
		genre: request.genre,
		dateOfBirth: request.dateOfBirth
    };
}
 
 /*
 Sending JWT back to the user, so it can be used to authenticate the user
 */
exports.login = function(req, res, next){
 
    var userInfo = setUserInfo(req.user);
 
    res.status(200).json({
        token: 'JWT ' + generateToken(userInfo),
        user: userInfo
    });
 
}
 
 /*
 Takes request and creates user while checking if the data is valid and email is not in the database already.
 */
exports.register = function(req, res, next){
 
    var email = req.body.email;
    var password = req.body.password;
    var role = req.body.role;
	var genre = req.body.genre;
	var dateOfBirth = req.body.dateOfBirth;
 
    if(!email){
        return res.status(422).send({error: 'You must enter an email address'});
    }
 
    if(!password){
        return res.status(423).send({error: 'You must enter a password'});
    }
	
	if(!dateOfBirth){
        return res.status(424).send({error: 'You must enter a year of birth'});
    }
 
    User.findOne({email: email}, function(err, existingUser){
 
        if(err){
            return next(err);
        }
 
        if(existingUser){
            return res.status(425).send({error: 'That email address is already in use'});
        }
 
        var user = new User({
            email: email,
            password: password,
            role: role,
			genre: genre,
			dateOfBirth: dateOfBirth
        });
 
        user.save(function(err, user){
 
            if(err){
                return next(err);
            }
 
            var userInfo = setUserInfo(user);
 
            res.status(201).json({
                token: 'JWT ' + generateToken(userInfo),
                user: userInfo
            })
 
        });
 
    });
 
}
 
 /*
 Authorizes by role, checks if the account has admin privilges.
 */
exports.roleAuthorization = function(roles){
 
    return function(req, res, next){
 
        var user = req.user;
 
        User.findById(user._id, function(err, foundUser){
 
            if(err){
                res.status(422).json({error: 'No user found.'});
                return next(err);
            }
 
            if(roles.indexOf(foundUser.role) > -1){
                return next();
            }
 
            res.status(401).json({error: 'You are not authorized to view this content'});
            return next('Unauthorized');
 
        });
 
    }
 
}