//Made using Josh Morony's tutorial - https://www.joshmorony.com/creating-role-based-authentication-with-passport-in-ionic-2-part-1/

var Todo = require('../models/todo');
 
 
 /*
 Returns all of the todos stored in the database
 */
exports.getTodos = function(req, res, next){
 
    Todo.find(function(err, todos) {
 
        if (err){
            res.send(err);
        }
 
        res.json(todos);
 
    });
 
}
 /*
 Function that will insert a new todo into the database and then return all of the todos that are currently in the database.
 */
exports.createTodo = function(req, res, next){
 
    Todo.create({
        title : req.body.title
    }, function(err, todo) {
 
        if (err){
            res.send(err);
        }
 
        Todo.find(function(err, todos) {
 
            if (err){
                res.send(err);
            }
 
            res.json(todos);
 
        });
 
    });
 
}
 
 /*
 Deleting specific todo from the database
 */
exports.deleteTodo = function(req, res, next){
 
    Todo.remove({
        _id : req.params.todo_id
    }, function(err, todo) {
        res.json(todo);
    });
 
}