//Made using Josh Morony's tutorial - https://www.joshmorony.com/creating-role-based-authentication-with-passport-in-ionic-2-part-1/

var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var methodOverride = require('method-override');
 
var databaseConfig = require('./config/database');
var router = require('./app/routes');
 
mongoose.connect(databaseConfig.url);
 
app.listen(process.env.PORT || 5000);
console.log("App listening on port 8080");
 
app.use(bodyParser.urlencoded({ extended: false })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(methodOverride());
app.use(cors());

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

var musicSchema = new mongoose.Schema({

    artist: String,
	name: String,
	youtubeID: String,
	genre: String,
	yearOfRelease: String,
	emotion: String,
	userRating: String
},{ collection : 'music' });

var Music = mongoose.model('Music',musicSchema);

// Get music
    app.get('/api/music', function(req, res) {
 
        console.log("fetching music");
 
        // use mongoose to get all music in the database
        Music.find(function(err, music) {
 
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)
 
            res.json(music); // return all music in JSON format
        });
    });
 
router(app);