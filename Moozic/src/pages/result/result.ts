import { Component } from '@angular/core';
import { NavController, NavParams, MenuController,LoadingController  } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions, Camera } from 'ionic-native';
import { StartPage } from '../start/start';
import { PlaybackPage } from '../playback/playback';
import { EmotionService } from '../../providers/EmotionService';
import { Music } from '../../providers/music';

/*
  Generated class for the Result page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
  providers: [EmotionService]
})
export class ResultPage {
	
	arr: any;
   randomNum1: any;
   randomNum2: any;
   randomNum3: any;
   songName1: string;
   songName2: string;
   songName3: string;
  youtubeID1: string;
  youtubeID2: string;
  youtubeID3: string;
  htmlString1: string;
  htmlString2: string;
  htmlString3: string;
  dominantEmotion: string;  
 public blob;  
  public base64Image: string;
  
  happiness1:string = "positive";
  happiness2:string = "joyous";
  happiness3:string = "euphoric";
  happiness4:string = "ecstatic";
  
  sadness1:string = "wistful";
  sadness2:string = "gloomy";
  sadness3:string = "depressed";
  sadness4:string = "tragic";
  
  anger1:string = "annoyed";
  anger2:string = "malevolent";
  anger3:string = "irritated";
  anger4:string = "outraged";
  
  fear1:string = "suspenseful";
  fear2:string = "spooky";
  fear3:string = "panic";
  fear4:string = "horror";
  
	  
  loading: any;
	music: any;
	emotionMusic: any;
	

  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, private emotionService: EmotionService, private musicService: Music, public loadingCtrl: LoadingController) {
	menu.enable(true); 
	
			this.dominantEmotion = this.navParams.get('emotion'); 
		
		this.musicService.getMusic().then((data) => {
      console.log(data);
      this.music = data;
		
		this.emotionMusic = this.getObjects(this.music,'',this.dominantEmotion);
		
		console.log(this.emotionMusic);
		
		console.log(this.randomNumber(3,this.emotionMusic.length - 1));
		
		this.arr = this.randomNumber(3,this.emotionMusic.length - 1);
	
		
		this.randomNum1 = this.arr[0];
		this.randomNum2 = this.arr[1];
		this.randomNum3 = this.arr[2];
		
    this.youtubeID1 = this.emotionMusic[this.randomNum1].youtubeID;  
    this.htmlString1 = '<div class="musicThumbnail"><img src="https://img.youtube.com/vi/' + this.youtubeID1 + '/hqdefault.jpg"></div>';
	this.youtubeID2 = this.emotionMusic[this.randomNum2].youtubeID;
    this.htmlString2 = '<div class="musicThumbnail2"><img src="https://img.youtube.com/vi/' + this.youtubeID2 + '/hqdefault.jpg"></div>';
	this.youtubeID3 = this.emotionMusic[this.randomNum3].youtubeID;
    this.htmlString3 = '<div class="musicThumbnail2"><img src="https://img.youtube.com/vi/' + this.youtubeID3 + '/hqdefault.jpg"></div>';
	
	this.songName1 = this.emotionMusic[this.randomNum1].artist + ' - ' + this.emotionMusic[this.randomNum1].name;
	this.songName2 = this.emotionMusic[this.randomNum2].artist + ' - ' + this.emotionMusic[this.randomNum2].name;
	this.songName3 = this.emotionMusic[this.randomNum3].artist + ' - ' + this.emotionMusic[this.randomNum3].name;
	
	
		
    });


	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');

	
	

	
  }
  
    /*
  Starting video recorder using MediaCapture plugin, native to Ionic framework.
  Setting the limit of videos to 1.
  */
  
  takePicture(){
    Camera.getPicture({
        destinationType: Camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000
    }).then((imageData) => {
        // imageData is a base64 encoded string
        this.base64Image = "data:image/jpeg;base64," + imageData;
		this.blob = this.makeblob(this.base64Image);
		this.showLoader();
		this.emotionService.post(this.blob).subscribe((data) => {
		
		console.log(data[0].scores); 
		
		var jsonData = data[0].scores;
		var maxProp = null
		var maxValue = -1
		for(var prop in jsonData){
			if(jsonData.hasOwnProperty(prop)){
				var value = jsonData[prop]
				if(value > maxValue){
					maxProp = prop
					maxValue = value
				}
			}	
		}
		
		if(maxProp == 'happiness' && maxValue <= 0.7)
		{
			maxProp = this.happiness1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue <= 0.85 && maxValue > 0.7)
		{
			maxProp = this.happiness2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue <= 0.95 && maxValue > 0.85)
		{
			maxProp = this.happiness3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue < 1.0 && maxValue > 0.95)
		{
			maxProp = this.happiness4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.6)
		{
			maxProp = this.sadness1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.sadness2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.sadness3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.sadness4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.6)
		{
			maxProp = this.anger1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.anger2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.anger3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.anger4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.6)
		{
			maxProp = this.fear1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.fear2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.fear3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.fear4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp !== 'fear' || maxProp !== 'happiness' || maxProp !== 'anger' || maxProp !== 'sadness' ){
			console.log(maxProp); 
			this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});		
			}
		
		});
		
		
    }, (err) => {
        console.log(err);
		this.navCtrl.setRoot(ResultPage);
		this.loading.dismiss();
    });
  }

    
     makeblob(dataURL) {
            var BASE64_MARKER = ';base64,';
            if (dataURL.indexOf(BASE64_MARKER) == -1) {
                var parts = dataURL.split(',');
                var contentType = parts[0].split(':')[1];
                var raw = decodeURIComponent(parts[1]);
                return new Blob([raw], { type: contentType });
            }
            var parts = dataURL.split(BASE64_MARKER);
            var contentType = parts[0].split(':')[1];
            var raw = window.atob(parts[1]);
            var rawLength = raw.length;

            var uInt8Array = new Uint8Array(rawLength);

            for (var i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }

            return new Blob([uInt8Array], { type: contentType });
        }
		

goToStartPage(){
	this.navCtrl.push(StartPage) && this.navCtrl.setRoot(StartPage);
	
}

goToPlaybackPage1(){
	let data = {
        youtubeID: this.youtubeID1
    };
	this.navCtrl.push(PlaybackPage, data)
	
}

goToPlaybackPage2(){
	let data = {
        youtubeID: this.youtubeID2
    };
	this.navCtrl.push(PlaybackPage, data)
}

goToPlaybackPage3(){
	let data = {
        youtubeID: this.youtubeID3
    };
	this.navCtrl.push(PlaybackPage, data)
}

		showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'Loading...'
        });
 
        this.loading.present();
 
    }
	
    getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));    
        } else 
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        } else if (obj[i] == val && key == ''){
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1){
                objects.push(obj);
            }
        }
    }
	
    return objects;
}


      randomNumber(amount, toNum) {
        var tempArray = [];
        var randNum = 0;
        while(tempArray.length < amount) {
                randNum = Math.ceil(Math.random() * toNum);
                if(tempArray.indexOf(randNum) > -1) {
                        continue;
                } else {
                        tempArray[tempArray.length] = randNum;
                }
        }
        return tempArray;
}




}

