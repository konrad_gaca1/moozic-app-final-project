import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { StartPage } from '../start/start';

/*
  Generated class for the Registration2 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registration2',
  templateUrl: 'registration2.html'
})
export class RegistrationPage2 {
	
	email: string;
  password: string;
  dateOfBirth: string;
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: Auth, public loadingCtrl: LoadingController) {}
  
  /*
  Getting the passed data from the previous Registration Page
  */

  ionViewDidLoad() {
    console.log('ionViewDidLoad Registration2Page');
	this.email = this.navParams.get('email');
	this.password = this.navParams.get('password');
	this.dateOfBirth = this.navParams.get('dateOfBirth');
  }
  
  
  /*
  Creating variable 'details' that contains data that the user inputted during the registration process.
  Sending the 'details' into createAccount() function in Auth provider in order to register the user in the databases.
  */
  
  register(){
 
    this.showLoader();
    let details = 'email=' + this.email + '&password=' + this.password + '&dateOfBirth=' + this.dateOfBirth;
 
    this.authService.createAccount(details).then((result) => {
      this.loading.dismiss();
      console.log(result);
      this.navCtrl.setRoot(StartPage);
    }, (err) => {
        this.loading.dismiss();
    });
 
  }
 
  showLoader(){
 
    this.loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });
 
    this.loading.present();
 
  }

}
