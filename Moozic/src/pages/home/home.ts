import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { StartPage } from '../start/start';
import { Auth } from '../../providers/auth';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	
	email: string;
    password: string;
    loading: any;

  constructor(public navCtrl: NavController, public authService: Auth, public loadingCtrl: LoadingController) {
	  
	 
 
  }
  
  /* Running an authentication checking method after the page loads.
  Showing Loader Animation while running authentication method.
  Displaying console message if user is authorized or not.
  */
  ionViewDidLoad() {
 
        this.showLoader();
 
        //Check if already authenticated
        this.authService.checkAuthentication().then((res) => {
            console.log("Already authorized");
            this.loading.dismiss();
            this.navCtrl.setRoot(StartPage);
        }, (err) => {
            console.log("Not already authorized");
            this.loading.dismiss();
        });
 
    }
	
	
	/* Logging function that sets variable 'credentials' with values of email and password that the user entered.
	   Then the credentials var is sent to Auth provider for authentication.
	*/
	
	login(){
 
        this.showLoader();
 
        let credentials = 'email=' + this.email + '&password=' + this.password;
 
        this.authService.login(credentials).then((result) => {
            this.loading.dismiss();
            console.log(result);
            this.navCtrl.setRoot(StartPage);
        }, (err) => {
            this.loading.dismiss();
            console.log(err);
        });
 
    }
	
	/*
	Function that creates loading animation
	*/
	showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'Authenticating...'
        });
 
        this.loading.present();
 
    }
  
  
  
  /*
  Navigating user to Registration Page.
  */
  goToRegistration(){
  this.navCtrl.push(RegistrationPage);
  
  }
  
  
  /*
  Navigating user to Start Page and setting the Start Page as a root page.
  */
  goToStart(){
  this.navCtrl.push(StartPage);
  this.navCtrl.setRoot(StartPage);
  
  }
  
  
  
}

