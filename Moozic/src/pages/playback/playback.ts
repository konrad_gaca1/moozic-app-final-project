import { Component} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StartPage } from '../start/start';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
/*
  Generated class for the Playback page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-playback',
  templateUrl: 'playback.html'
})
export class PlaybackPage {
	
	youtubeID: string;
	musicVideo: string;
	videoUrl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sanitizer: DomSanitizer) {
  
  this.youtubeID = 'https://www.youtube.com/embed/' + this.navParams.get('youtubeID');
  
  this.videoUrl =
      this.sanitizer.bypassSecurityTrustResourceUrl(this.youtubeID);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaybackPage');
	
  }
  
   //this.musicVideo = '<object width="420" height="315" class="musicVideo" data="https://www.youtube.com/v/' + this.youtubeID + '"></object>'; //nie dziala
  
  //this.musicVideo = '<iframe width="260" height="200" class="musicVideo" src="https://www.youtube.com/embed/' + this.youtubeID + '"></iframe>'; //nie dziala
  
  goToStartPage(){
	this.navCtrl.push(StartPage) && this.navCtrl.setRoot(StartPage);
	
}

}
