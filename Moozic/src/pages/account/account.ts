import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/*
  Generated class for the Account page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  
  
  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Remove Account',
      message: 'Are you sure ?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log('Account removed!');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('Good.');
          }
        }
      ]
    });
    confirm.present();
  }
  
  
  
  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Change Password',
      message: "",
      inputs: [
        {
          name: 'oldPassword1',
          placeholder: 'Old Password'
        },
		{
          name: 'oldPassword2',
          placeholder: 'Old Password Again'
        },
		{
          name: 'newPassword',
          placeholder: 'New Password'
        },
      ],
      buttons: [
        {
          text: 'Confirm',
          handler: data => {
            console.log('Confirm clicked');
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    prompt.present();
  }

}
