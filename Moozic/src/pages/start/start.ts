import { Component } from '@angular/core';
import { App, NavController, NavParams, MenuController,LoadingController } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions, Camera } from 'ionic-native';
import { ResultPage } from '../result/result';
import { EmotionService } from '../../providers/EmotionService';

/*
  Generated class for the Start page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
  providers: [EmotionService]
})
export class StartPage {

 public base64Image: string;
 public blob;
 
  happiness1:string = "positive";
  happiness2:string = "joyous";
  happiness3:string = "euphoric";
  happiness4:string = "ecstatic";
  
  sadness1:string = "wistful";
  sadness2:string = "gloomy";
  sadness3:string = "depressed";
  sadness4:string = "tragic";
  
  anger1:string = "annoyed";
  anger2:string = "malevolent";
  anger3:string = "irritated";
  anger4:string = "outraged";
  
  fear1:string = "suspenseful";
  fear2:string = "spooky";
  fear3:string = "panic";
  fear4:string = "horror";
  
  loading: any;

  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, private emotionService: EmotionService, public loadingCtrl: LoadingController) {menu.enable(true);}

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartPage');
  }
  
  
  /*
  Starting video recorder using MediaCapture plugin, native to Ionic framework.
  Setting the limit of videos to 1.
  */
  
   /*startVideoCapture(){
 
	 
let options: CaptureVideoOptions = { limit: 1, duration: 3};
MediaCapture.captureVideo(options)
  .then(
    (data: MediaFile[]) => this.navCtrl.push(ResultPage) && this.navCtrl.setRoot(ResultPage),
    //(err: CaptureError) => this.navCtrl.push(StartPage) && this.navCtrl.setRoot(StartPage)
	//For testing
	(err: CaptureError) => this.navCtrl.push(ResultPage) && this.navCtrl.setRoot(ResultPage)
  ).catch((err: CaptureError) => this.navCtrl.push(StartPage) && this.navCtrl.setRoot(StartPage));
  
}*/

test(){
 
	 
this.navCtrl.push(ResultPage) && this.navCtrl.setRoot(ResultPage)

  
}


  takePicture(){
    Camera.getPicture({
        destinationType: Camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000
    }).then((imageData) => {
        // imageData is a base64 encoded string
        this.base64Image = "data:image/jpeg;base64," + imageData;
		this.blob = this.makeblob(this.base64Image);
		this.showLoader();
		this.emotionService.post(this.blob).subscribe((data) => {
		
		console.log(data[0].scores); 
		
		var jsonData = data[0].scores;
		var maxProp = null
		var maxValue = -1
		for(var prop in jsonData){
			if(jsonData.hasOwnProperty(prop)){
				var value = jsonData[prop]
				if(value > maxValue){
					maxProp = prop
					maxValue = value
				}
			}	
		}
		
		if(maxProp == 'happiness' && maxValue <= 0.7)
		{
			maxProp = this.happiness1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue <= 0.85 && maxValue > 0.7)
		{
			maxProp = this.happiness2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue <= 0.95 && maxValue > 0.85)
		{
			maxProp = this.happiness3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'happiness' && maxValue < 1.0 && maxValue > 0.95)
		{
			maxProp = this.happiness4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.6)
		{
			maxProp = this.sadness1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.sadness2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.sadness3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'sadness' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.sadness4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.6)
		{
			maxProp = this.anger1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.anger2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.anger3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'anger' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.anger4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.6)
		{
			maxProp = this.fear1;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.7 && maxValue > 0.6)
		{
			maxProp = this.fear2;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue <= 0.8 && maxValue > 0.7)
		{
			maxProp = this.fear3;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp == 'fear' && maxValue < 1.0 && maxValue > 0.8)
		{
			maxProp = this.fear4;
			console.log(maxProp); 
				this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});
		}
		else if(maxProp !== 'fear' || maxProp !== 'happiness' || maxProp !== 'anger' || maxProp !== 'sadness' ){
			console.log(maxProp); 
			this.loading.dismiss();
			this.navCtrl.setRoot(ResultPage,{emotion: maxProp});		
			}
		
		});
		
		
    }, (err) => {
        console.log(err);
		this.navCtrl.setRoot(ResultPage);
		this.loading.dismiss();
    });
  }

    
     makeblob(dataURL) {
            var BASE64_MARKER = ';base64,';
            if (dataURL.indexOf(BASE64_MARKER) == -1) {
                var parts = dataURL.split(',');
                var contentType = parts[0].split(':')[1];
                var raw = decodeURIComponent(parts[1]);
                return new Blob([raw], { type: contentType });
            }
            var parts = dataURL.split(BASE64_MARKER);
            var contentType = parts[0].split(':')[1];
            var raw = window.atob(parts[1]);
            var rawLength = raw.length;

            var uInt8Array = new Uint8Array(rawLength);

            for (var i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }

            return new Blob([uInt8Array], { type: contentType });
        }
		
		showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'Loading...'
        });
 
        this.loading.present();
 
    }


}
