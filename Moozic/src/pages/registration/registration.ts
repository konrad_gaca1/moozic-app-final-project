import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RegistrationPage2 } from '../registration2/registration2';

/*
  Generated class for the Registration page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {
	
  email: string;
  password: string;
  dateOfBirth: string;
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }
  
  /*
  Navigating user to the second Registration Page as well as passing data into that page.
  */
  goToRegistration2(){
	  
	  let data = {
        email: this.email,
        password: this.password,
        dateOfBirth: this.dateOfBirth
    };
  this.navCtrl.push(RegistrationPage2, data);

}
}
