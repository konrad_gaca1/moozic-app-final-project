import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/*
  Generated class for the Preferences page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-preferences',
  templateUrl: 'preferences.html'
})
export class PreferencesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferencesPage');
  }
  
    showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Change Preferences',
      message: 'Are you sure ?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log('Preferences changed!');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('Good.');
          }
        }
      ]
    });
    confirm.present();
  }

}
