import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegistrationPage } from '../pages/registration/registration';
import { RegistrationPage2 } from '../pages/registration2/registration2';
import { StartPage } from '../pages/start/start';
import { SettingsPage } from '../pages/settings/settings';
import { PreferencesPage } from '../pages/preferences/preferences';
import { AccountPage } from '../pages/account/account';
import { Auth } from '../providers/auth';
import { Todos } from '../providers/todos';
import { Music } from '../providers/music';
import { ResultPage } from '../pages/result/result';
import { PlaybackPage } from '../pages/playback/playback';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
	RegistrationPage,
	RegistrationPage2,
	StartPage,
	SettingsPage,
	PreferencesPage,
	AccountPage,
	ResultPage,
	PlaybackPage
	
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
	RegistrationPage,
	RegistrationPage2,
	StartPage,
	SettingsPage,
	PreferencesPage,
	AccountPage,
	ResultPage,
	PlaybackPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Storage, Auth, Todos, Music]
})
export class AppModule {}
