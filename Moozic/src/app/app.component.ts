import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Auth } from '../providers/auth';

import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';
import { PreferencesPage } from '../pages/preferences/preferences';
import { AccountPage } from '../pages/account/account';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon: any}>;
  
  constructor(public platform: Platform, public auth: Auth) {
	  
	  
	  this.initializeApp();
	  
	// used for an example of ngFor and navigation
    this.pages = [
      { title: 'Preferences', component: PreferencesPage, icon:'img/ios7-settings.png' },
      { title: 'Account', component: AccountPage, icon:'img/android-social-user.png' },
	  { title: 'About', component: SettingsPage, icon:'img/information-circled.png' }
    ];
	
  }
  
  
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
  
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
  
  logout(){
    this.auth.logout();
  }
  
   testLogout(){
    this.logout();
	this.nav.setRoot(HomePage);
  }
  
  
}
